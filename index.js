const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
const db = require('./queries')

app.use(bodyParser.json())

app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

app.get('/', (req,res) => { 
    res.json({info:'App Running!!!'});
})

app.listen(port, () => {
    console.log('Express App running on 3000')
})

app.get("/messages", db.getMessages);
app.post("/messages", db.createMessage);